import parkingWidget from '../../src/parking-widget';
import dummyData from '../setup/model.json';

describe('park widget', () => {
  describe('getSelectedSlot() function', () => {
    beforeEach(() => {
      document.body.innerHTML = '<div id="testParkingWidget"></div>';
      parkingWidget.init(document.getElementById('testParkingWidget'), dummyData.vendor, dummyData.parkings, dummyData.dictionary);
      spy(parkingWidget, 'getSelectedSlot');
      parkingWidget.getSelectedSlot();
    });

    it('should have been run once', () => {
      expect(parkingWidget.getSelectedSlot).to.have.been.calledOnce;
    });

    it('should return null default', () => {
      expect(parkingWidget.getSelectedSlot).to.have.returned.null;
    });
    
    it('should return the first clicked with id 123', () => {
      var evt = document.createEvent('HTMLEvents');
      evt.initEvent('click', false, true);
      document.querySelector('.slot__container').dispatchEvent(evt);
      expect(parkingWidget.getSelectedSlot()).to.have.property('id')
        .that.equals(123);
    });
  });

});
