import './parking-widget.css';
import {
  toggleClass,
  removeClass,
  addEventListenerAll
}
from './dom-utils';
import pLog from './prefixed-log';
const log = pLog('*** ParkingWidget - ');

let _domEl;
let _vendor;
let _slots;
let _dictionary;
let _selectedSlot;

function getVendorTemplate() {
  log('Get vendor template with: ', _vendor);
  return `<div class="vendor vendor__container">
    <div class="vendor__info">
      <p class="vendor__name">${_vendor.name}</p>
      <p class="vendor__slogan">${_dictionary.bookYourParking} ${_vendor.departureAirport}</p>
    </div>
    <div class="vendor__actions">
      <img class="vendor__logo" src="${_vendor.img}" alt="Logo of ${_vendor.name}"/> 
      <button class="vendor__button vendor_showMore">Mostra Dettagli</button>
    </div>
    <div class="vendor__slots vendor__slots--hidden">
      <div class="vendor__map"><img src="${_vendor.map}" alt="Map of ${_vendor.name}"/></div>
      <div class="vendor__features">
        <p class="vendor__featuresTitle">${_dictionary.featuresTitle}</p>
        <ul class="vendor__featuresList">${getVendorFeaturesTemplate()}</ul>
        <p class="vendor__featuresTitle">Seleziona un parcheggio: </p>
        <div class="vendor__slots__container">${getSlotsTemplate()}</div>
      </div>
    </div>
  </div>`;
}

function getSlotsTemplate() {
  log('Get slots template with: ', _slots);
  return _slots.map((slot) => {
    return `<div class="slot slot__container" data-id="${slot.id}">
      <p class="slot__feature">${slot.indoor ? _dictionary.indoorSpace : _dictionary.outdoorSpace}</p>
      <p class="slot__feature">${slot.insurance ? _dictionary.insuranceIncluded : _dictionary.insuranceExcluded}</p>
      <p class="slot__price">${slot.price}</p>
    </div>`;
  }).join('');
}

function getVendorFeaturesTemplate() {
  return _vendor.features.map((feature) => {
    return `<li class="vendor__feature">${feature}</li>`;
  }).join('');
}

function attachEvents() {
  _domEl.querySelector('.vendor_showMore').addEventListener('click', function(event) {
    toggleClass(_domEl.children[0].querySelector('.vendor__slots'), 'vendor__slots--hidden');
  });

  Array.from(_domEl.querySelectorAll('.slot__container')).forEach(slotContainer => {
    slotContainer.addEventListener('click', function(event) {
      let selectedClass = 'slot__container--selected';
      removeClass(_selectedSlot, selectedClass);
      if (this != _selectedSlot) {
        _selectedSlot = this;
        toggleClass(_selectedSlot, selectedClass);
      }
      else {
        _selectedSlot = null;
      }
    });
  });
};

const parkingWidget = {
  init(domEl, vendor, slots, dictionary) {
    _domEl = domEl;
    _vendor = vendor;
    _slots = slots;
    _dictionary = dictionary;
    _domEl.innerHTML = getVendorTemplate();
    attachEvents();
    return this;
  },
  getSelectedSlot() {
    return _slots.filter(slot => _selectedSlot && slot.id == _selectedSlot.getAttribute('data-id'))[0];
  }
};

export default parkingWidget;
