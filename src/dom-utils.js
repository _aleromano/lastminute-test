import dLog from './prefixed-log';
const log = dLog('___ DOM Utility');

function toggleClass(el, className) {
  log(`toggleClass [${className}] to {${el} with class [${el.className}]}`);
  if (el.classList) {
    el.classList.toggle(className);
  }
  else {
    let classes = el.className.split(' ');
    let existingIndex = classes.indexOf(className);

    if (existingIndex >= 0) {
      classes.splice(existingIndex, 1);
    }
    else {
      classes.push(className);
    }

    el.className = classes.join(' ');
  }
}

function removeClass(el, className) {
  log(`removeClass [${className}] from el}`);
  if (el) {
    if (el.classList) {
      el.classList.remove(className);
    }
    else {
      el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }
}

function hasClass(el, className) {
  return el.classList.contains(className);
}

function addEventListenerAll(elems, eventType, callback) {
  Array.from(elems).forEach(el => {
    el.addEventListener(eventType, callback.bind(el));
  });

}

export {
  toggleClass,
  removeClass,
  hasClass,
  addEventListenerAll
};
