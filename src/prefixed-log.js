export default function log(prefix) {
  return function(...args) { console.log(prefix, ...args); };
}
