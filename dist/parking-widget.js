(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["parkingWidget"] = factory();
	else
		root["parkingWidget"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	__webpack_require__(1);
	
	var _domUtils = __webpack_require__(5);
	
	var _prefixedLog = __webpack_require__(6);
	
	var _prefixedLog2 = _interopRequireDefault(_prefixedLog);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var log = (0, _prefixedLog2.default)('*** ParkingWidget - ');
	
	var _domEl = void 0;
	var _vendor = void 0;
	var _slots = void 0;
	var _dictionary = void 0;
	var _selectedSlot = void 0;
	
	function getVendorTemplate() {
	  log('Get vendor template with: ', _vendor);
	  return '<div class="vendor vendor__container">\n    <div class="vendor__info">\n      <p class="vendor__name">' + _vendor.name + '</p>\n      <p class="vendor__slogan">' + _dictionary.bookYourParking + ' ' + _vendor.departureAirport + '</p>\n    </div>\n    <div class="vendor__actions">\n      <img class="vendor__logo" src="' + _vendor.img + '" alt="Logo of ' + _vendor.name + '"/> \n      <button class="vendor__button vendor_showMore">Mostra Dettagli</button>\n    </div>\n    <div class="vendor__slots vendor__slots--hidden">\n      <div class="vendor__map"><img src="' + _vendor.map + '" alt="Map of ' + _vendor.name + '"/></div>\n      <div class="vendor__features">\n        <p class="vendor__featuresTitle">' + _dictionary.featuresTitle + '</p>\n        <ul class="vendor__featuresList">' + getVendorFeaturesTemplate() + '</ul>\n        <p class="vendor__featuresTitle">Seleziona un parcheggio: </p>\n        <div class="vendor__slots__container">' + getSlotsTemplate() + '</div>\n      </div>\n    </div>\n  </div>';
	}
	
	function getSlotsTemplate() {
	  log('Get slots template with: ', _slots);
	  return _slots.map(function (slot) {
	    return '<div class="slot slot__container" data-id="' + slot.id + '">\n      <p class="slot__feature">' + (slot.indoor ? _dictionary.indoorSpace : _dictionary.outdoorSpace) + '</p>\n      <p class="slot__feature">' + (slot.insurance ? _dictionary.insuranceIncluded : _dictionary.insuranceExcluded) + '</p>\n      <p class="slot__price">' + slot.price + '</p>\n    </div>';
	  }).join('');
	}
	
	function getVendorFeaturesTemplate() {
	  return _vendor.features.map(function (feature) {
	    return '<li class="vendor__feature">' + feature + '</li>';
	  }).join('');
	}
	
	function attachEvents() {
	  _domEl.querySelector('.vendor_showMore').addEventListener('click', function (event) {
	    (0, _domUtils.toggleClass)(_domEl.children[0].querySelector('.vendor__slots'), 'vendor__slots--hidden');
	  });
	
	  Array.from(_domEl.querySelectorAll('.slot__container')).forEach(function (slotContainer) {
	    slotContainer.addEventListener('click', function (event) {
	      var selectedClass = 'slot__container--selected';
	      (0, _domUtils.removeClass)(_selectedSlot, selectedClass);
	      if (this != _selectedSlot) {
	        _selectedSlot = this;
	        (0, _domUtils.toggleClass)(_selectedSlot, selectedClass);
	      } else {
	        _selectedSlot = null;
	      }
	    });
	  });
	};
	
	var parkingWidget = {
	  init: function init(domEl, vendor, slots, dictionary) {
	    _domEl = domEl;
	    _vendor = vendor;
	    _slots = slots;
	    _dictionary = dictionary;
	    _domEl.innerHTML = getVendorTemplate();
	    attachEvents();
	    return this;
	  },
	  getSelectedSlot: function getSelectedSlot() {
	    return _slots.filter(function (slot) {
	      return _selectedSlot && slot.id == _selectedSlot.getAttribute('data-id');
	    })[0];
	  }
	};
	
	exports.default = parkingWidget;
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(2);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(4)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../node_modules/css-loader/index.js!./parking-widget.css", function() {
				var newContent = require("!!./../node_modules/css-loader/index.js!./parking-widget.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(3)();
	// imports
	
	
	// module
	exports.push([module.id, ".vendor {\n    border: 5px solid #dedede;\n    padding: 5px;\n    font-family: 'Lato', sans-serif;\n    width: 90%;\n    margin: 0 auto;\n    overflow: hidden;\n}\n\n.vendor__container--expanded {\n    height: auto;\n    overflow: auto;\n}\n\n.vendor__info {\n    width: calc(70% - 5px);\n    float: left;\n}\n\n.vendor__actions {\n    width: calc(30% - 5px);\n    display: inline-block;\n    text-align: right;\n}\n\n.vendor__name {\n    font-family: inherit;\n    font-weight: bold;\n    font-size: 1.7em;\n    margin: 0;\n    padding: 5px;\n}\n\n.vendor__slogan {\n    font-family: inherit;\n    font-style: italic;\n    font-size: 1.4em;\n    margin: 0;\n    padding: 5px;\n}\n\n.vendor__logo {\n    width: 175px;\n    display: block;\n    margin-left: calc(100% - 175px);\n}\n\n.vendor__button {\n    background: blue;\n    color: white;\n    text-transform: uppercase;\n    font-weight: bold;\n    padding: 10px;\n    text-align: center;\n    font: inherit;\n    border: 1px solid navy;\n    cursor: pointer;\n}\n\n.vendor__slots {\n    max-height: 9999px;\n    transition-timing-function: cubic-bezier(0.5, 0, 1, 0); \n    transition-delay: 0s;\n    margin-top: 20px;\n    overflow: auto;\n}\n\n.vendor__slots--hidden {\n    max-height: 0;\n    transition: max-height 0.75s cubic-bezier(0, 1, 0, 1) -.1s;\n    overflow: hidden;\n}\n\n.vendor__map {\n    width: 375px;\n    height: 390px;\n    overflow: hidden;\n    float: left;\n}\n\n.vendor__features {\n    width: calc(100% - 385px);\n    margin-left: 10px;\n    float: left;\n}\n.vendor__featuresTitle {\n    text-decoration: underline;\n    font-weight: bold;\n}\n\n.vendor__featuresList {\n    padding-left: 20px;\n    text-indent: 2px;\n    list-style: none;\n    list-style-position: outside;\n}\n\n.vendor__featuresList li:before {\n    content: '\\2714';\n    margin-left: -1em;\n    margin-right: .35em;\n    color: green;\n}\n\n.vendor__slots__container {\n    width: 100%;\n    clear: both;\n    overflow: auto;\n}\n\n.slot__container {\n    border: 1px solid navy;\n    width: calc(46% - 10px);\n    float: left;\n    margin: 5px;\n    padding: 5px;\n    cursor: pointer;\n}\n\n.slot__container:hover:not(.slot__container--selected) {\n    background-color: deepskyblue;\n}\n\n.slot__container--selected {\n    background: blue;\n    color: white;\n}\n\n.slot__feature::before {\n    content: '\\2708';\n    margin-right: .35em;\n}\n\n.slot__price {\n    float: right;\n    font-weight: bold;\n}\n\n@media(max-width: 800px) {\n    .vendor__info {\n        width: 100%;\n        float: none;\n    }\n    .vendor__actions {\n        width: 100;\n    }\n    .vendor__logo {\n        width: 100%;\n        display: block;\n        margin-left: 0;\n    }\n    \n    .vendor__map {\n        width: 100%;\n        float: none;\n    }\n    .vendor__features {\n        width: 100%;\n        margin: 0;\n        float: none;\n    }\n    .slot__container {\n        width: 90%;\n        margin: .7em auto;\n        float: none;\n        overflow: auto;\n    }\n    .slot__feature {\n        margin: 5px auto;\n    }\n}\n", ""]);
	
	// exports


/***/ },
/* 3 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.addEventListenerAll = exports.hasClass = exports.removeClass = exports.toggleClass = undefined;
	
	var _prefixedLog = __webpack_require__(6);
	
	var _prefixedLog2 = _interopRequireDefault(_prefixedLog);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var log = (0, _prefixedLog2.default)('___ DOM Utility');
	
	function toggleClass(el, className) {
	  log('toggleClass [' + className + '] to {' + el + ' with class [' + el.className + ']}');
	  if (el.classList) {
	    el.classList.toggle(className);
	  } else {
	    var classes = el.className.split(' ');
	    var existingIndex = classes.indexOf(className);
	
	    if (existingIndex >= 0) {
	      classes.splice(existingIndex, 1);
	    } else {
	      classes.push(className);
	    }
	
	    el.className = classes.join(' ');
	  }
	}
	
	function removeClass(el, className) {
	  log('removeClass [' + className + '] from el}');
	  if (el) {
	    if (el.classList) {
	      el.classList.remove(className);
	    } else {
	      el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
	    }
	  }
	}
	
	function hasClass(el, className) {
	  return el.classList.contains(className);
	}
	
	function addEventListenerAll(elems, eventType, callback) {
	  Array.from(elems).forEach(function (el) {
	    el.addEventListener(eventType, callback.bind(el));
	  });
	}
	
	exports.toggleClass = toggleClass;
	exports.removeClass = removeClass;
	exports.hasClass = hasClass;
	exports.addEventListenerAll = addEventListenerAll;

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = log;
	function log(prefix) {
	  return function () {
	    var _console;
	
	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }
	
	    (_console = console).log.apply(_console, [prefix].concat(args));
	  };
	}
	module.exports = exports["default"];

/***/ }
/******/ ])
});
;
//# sourceMappingURL=parking-widget.js.map